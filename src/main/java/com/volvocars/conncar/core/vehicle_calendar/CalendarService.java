package com.volvocars.conncar.core.vehicle_calendar;

import java.util.Date;
import java.util.concurrent.Callable;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.api.CalendarApi;
import io.swagger.api.NotFoundException;
import io.swagger.model.Entry;

@RestController
public class CalendarService implements CalendarApi {

	@Override
	public Callable<ResponseEntity<Entry>> getEntry(@PathVariable("vin") String vin, @PathVariable("id") String id) throws NotFoundException {
		System.out.println("vin " + vin);
		System.out.println("id" + id);

		return () -> {
			Entry entry = new Entry();
			entry.setData("asdfasdf");
			entry.setStartTime(new Date());
			entry.setEndTime(new Date());
			entry.setExclusive(true);
			entry.setFunction("function data");
			return new ResponseEntity<>(entry, HttpStatus.OK);
		};
	}
}
