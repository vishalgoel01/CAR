package com.volvocars.conncar.core.vehicle_calendar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleCalendarStartup {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(VehicleCalendarStartup.class, args);
	}
}